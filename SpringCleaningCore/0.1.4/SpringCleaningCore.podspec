Pod::Spec.new do |spec|
  spec.name         = "SpringCleaningCore"
  spec.version      = "0.1.4"
  spec.summary      = "Core Framework behind Spring Cleaning apps"

  spec.homepage     = "https://bitbucket.org/jacobrelkin/springcleaningcore"
  spec.author       = { "Jacob Relkin" => "plantpurecode@gmail.com" }

  spec.ios.deployment_target = "13.0"
  spec.module_name  = "SpringCleaningCore"

  spec.source       = { :git => "git@bitbucket.org:jacobrelkin/springcleaningcore.git", :tag => "#{spec.version}" }
  spec.source_files = "SpringCleaningCore/**/*.swift"

  spec.dependency    "PanModal", "1.2.7"
  spec.dependency    "SwiftyStoreKit", "0.16.1"

  spec.frameworks    = "Contacts", "ContactsUI"
  spec.swift_version = "5.0"

  spec.resources = "#{spec.name}/**/*.{png,jpeg,jpg,dat,storyboard,xib,xcassets}"
end
